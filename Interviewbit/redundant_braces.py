class Solution:
    # @param A : string
    # @return an integer
    def braces(self, A):
        stack = []
        for itr in A:
            if itr == "(":
                stack.append(itr)
            elif itr == "+" or itr == "-" or itr == "*" or itr == "/":
                stack.append("exp")
            elif itr == ")":
                if stack[-1] == "exp":
                    stack.pop()
                    stack.pop()
                else:
                    return 1
        return 0

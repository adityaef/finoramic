class Solution:
    def threeSumClosest(self, arr,target):
        i = 0
        arr = sorted(arr)
        min_ = float('inf')
        n = len(arr)
        res= 0
        while (i < n):
            j = i+1
            k = n-1
            while (j<k):
                sum_ = arr[i]+arr[j]+arr[k]
                diff = abs(sum_ - target)
                if diff == 0:
                    return sum_
                if diff < min_:
                    min_ = diff
                    res = sum_
                if sum_ <= target:
                    j += 1
                else:
                    k -= 1
            i += 1
        return res

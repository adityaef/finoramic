class Solution:
    # @param A : tuple of strings
    # @return a list of list of integers
    def anagrams(self, arr):

        from collections import defaultdict
        lookup_table = defaultdict(list)

        result = []

        for i, word in enumerate(arr):
            lookup_table["".join(sorted(word))].append(i + 1)

        for k in lookup_table.values():
            result.append(k)
        return result
